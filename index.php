<?php
/** 
 * Created by PhpStorm.
 * User: 王磊
 * Date: 2017-02-10
 * Time: 11:46
 */
 
/**
 * @param $dir   目录
 * @param int $mode 目录权限
 * @return bool  返回成功失败
 */
function mkdirs($dir, $mode = 0777)
{
    if (is_dir($dir) || @mkdir($dir, $mode)) return TRUE;
    if (!mkdirs(dirname($dir), $mode)) return FALSE;
    return @mkdir($dir, $mode);
}


/**
 * @param $data  二维码参数
 * @param $table  数据库表名或者理解为图片目录
 * @return string  图片路径
 */
function create_erweima($data, $table)
{
    require("./phpqrcode/phpqrcode.php");
    // 纠错级别：L、M、Q、H
    $level = 'H';
    // 点的大小：1到10,用于手机端4就可以了
    $size = 8;
    // 下面注释了把二维码图片保存到本地的代码,如果要保存图片,用$fileName替换第二个参数false
    $path = "images/" . $table . "/";
    mkdirs($path);
    // 生成的文件名
    $fileName = $path . $data . '.png';  //如果存在不重新生成
    if (!file_exists($fileName)) {        //这里是判断文件是否存在，如果有数据库应该判断数据库的图片字段是否存在，存在返回图片路径，否则生成图片，然后存入该字段
        QRcode::png($data, $fileName, $level, $size);     //生成图片文件   不传$fileName输出图片文件
    }


    return $fileName;
}


echo 6666;die;

$data = @$_GET['data'];
$table = @$_GET['table'];
?>


<img src="<?php echo create_erweima($data, $table); ?>">